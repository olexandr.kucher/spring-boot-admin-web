FROM openjdk:15-alpine

ENV MAX_RAM_PERCENTAGE=80 \
  ADDITIONAL_JAVA_FLAGS="" \
  JAVA_ARGS="" \
  DEBUG_ARGS="" \
  APPLICATION_ARGS=""

ADD /spring-boot-admin/build/libs/*.jar spring-boot-admin.jar

EXPOSE 9001

ENTRYPOINT java \
  -XX:+UseContainerSupport \
  -XX:MaxRAMPercentage=$MAX_RAM_PERCENTAGE \
  $ADDITIONAL_JAVA_FLAGS \
  $JAVA_ARGS \
  $DEBUG_ARGS \
  -jar /spring-boot-admin.jar \
  $APPLICATION_ARGS