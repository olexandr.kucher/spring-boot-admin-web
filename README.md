# Spring Boot Admin Server
#### Simple [Spring Boot Admin Server](https://codecentric.github.io/spring-boot-admin/2.4.1/) application
#### This application provides a user-friendly UI for Spring Boot Actuator endpoints

* Main application properties:
    * _server.port_ - HTTP port of the application (default: _9001_)
    * _spring.application.name_ - Java Spring application name (default: _spring-boot-admin_)
* By default, application writes logs into **console** only

## Supported tags and respective `Dockerfile` links

### Simple Tags
* [**master**](https://gitlab.com/olexandr.kucher/spring-boot-admin-web/-/blob/master/Dockerfile) - the latest image build from the master branch of the project

## How to use this image

### Run Spring Boot Admin Server

```shell
docker container run --name spring-boot-admin -p 9001:9001 -d olexandrkucher/spring-boot-admin:master
```

## Authors
* [**Oleksandr Kucher**](https://gitlab.com/olexandr.kucher) - _Initial development_

## License
This project is licensed under the MIT License - see the [LICENSE](https://gitlab.com/olexandr.kucher/spring-boot-admin-web/-/blob/master/LICENSE) file for details.